use yew::prelude::{html, Component, ComponentLink, Html, KeyboardEvent, Properties, ShouldRender};

use crate::components::{
    form::Input,
    todo::{List, ListItem},
};

use crate::app::AppFilter;

#[derive(Ord, PartialOrd, Eq, PartialEq)]
struct ItemData {
    pub id: u32,
    pub name: String,
    pub complete: bool,
}

#[derive(Properties, Clone)]
pub struct IndexProps {
    pub filter: AppFilter,
}

pub struct Index {
    link: ComponentLink<Self>,
    props: IndexProps,
    current_todo: String,
    items: Vec<ItemData>,
    internal_id: u32,
}

pub enum IndexMsg {
    InputChange(String),
    Keypress(u32),
    ToggleComplete(u32),
    RemoveItem(u32),
}

pub enum Keycode {
    Enter = 13,
}

fn is_keycode(value: u32, code: Keycode) -> bool {
    value == code as u32
}

impl Component for Index {
    type Message = IndexMsg;
    type Properties = IndexProps;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            props,
            current_todo: String::new(),
            items: Vec::new(),
            internal_id: 0,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            IndexMsg::InputChange(input) => {
                self.current_todo = input;
            }
            IndexMsg::ToggleComplete(item_id) => {
                for item in &mut self.items {
                    if item.id == item_id {
                        item.complete = !item.complete;
                    }
                }
            }
            IndexMsg::RemoveItem(item_id) => {
                self.items.retain(|item| item.id != item_id);
            }
            IndexMsg::Keypress(keycode) => match keycode {
                _ if is_keycode(keycode, Keycode::Enter) => {
                    let name = self.current_todo.clone();
                    self.current_todo = String::new();

                    if !name.is_empty() {
                        self.items.push(ItemData {
                            id: self.internal_id,
                            name,
                            complete: false,
                        });

                        self.internal_id += 1;
                    }
                }
                _ => {}
            },
        }

        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        if self.props.filter != props.filter {
            self.props.filter = props.filter;
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        let items = self.render_items(&self.props.filter);

        html! {
            <>
                <header
                    class="header"
                    onkeypress=self.link.callback(|e: KeyboardEvent| IndexMsg::Keypress(e.key_code()))
                >
                    <h1>{ "todos" }</h1>
                    <Input
                        class="new-todo"
                        value=self.current_todo.clone()
                        placeholder="what needs to be done?"
                        handle_change=self.link.callback(IndexMsg::InputChange)
                    />
                </header>
                <section class="main">
                    <List class="todo-list">
                        { items }
                    </List>
                </section>
            </>
        }
    }
}

impl Index {
    fn render_items(&self, filter: &AppFilter) -> Vec<Html> {
        self.items
            .iter()
            .filter(|item| match filter {
                AppFilter::Active => !item.complete,
                AppFilter::Complete => item.complete,
                AppFilter::All => true,
            })
            .map(|litem| {
                let ItemData { name, id, complete } = litem;

                html! {
                    <ListItem
                        key={ *id as i128 }
                        id=id
                        class="todo"
                        item=name
                        complete=complete
                        handle_complete=self.link.callback(IndexMsg::ToggleComplete)
                        handle_remove=self.link.callback(IndexMsg::RemoveItem)
                    />
                }
            })
            .collect::<Vec<Html>>()
    }
}
