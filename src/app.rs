use yew::prelude::{html, Component, ComponentLink, Html, ShouldRender};

use crate::components::Filters;
use crate::routes::router;

#[derive(Copy, Clone, PartialEq)]
pub enum AppFilter {
    All,
    Active,
    Complete,
}

pub struct App {
    link: ComponentLink<Self>,
    item_count: u32,
}

impl Component for App {
    type Message = ();
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            item_count: 0, // set initial state to 0
        }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <div id="todo-app" class="todomvc-wrapper">
                <section class="todoapp">
                    { router() }
                    <footer class="footer">
                        <span class="todo-count">
                            <strong>{self.item_count}</strong>
                            {" items(s) left"}
                        </span>
                        <Filters />
                    </footer>
                </section>
                <footer class="info"/>
            </div>
        }
    }
}
