// use yew_router::prelude::*;
// use yew_router::switch::Permissive;

// pub mod about;
// pub mod home;

// /// App routes
// #[derive(Switch, Debug, Clone)]
// pub enum AppRoute {
//     #[to = "/about"]
//     About,
//     #[to = "/page-not-found"]
//     PageNotFound(Permissive<String>),
//     #[to = "/"]
//     Home,
// }

use yew::prelude::{html, Html};
use yew_router::{router::Router, Switch};

use crate::app::AppFilter;
use crate::views::Index;

#[derive(Switch, Clone)]
pub enum AppRoute {
    #[to = "/complete"]
    Complete,
    #[to = "/active"]
    Active,
    #[to = "/"]
    Index,
}

pub fn router() -> Html {
    html! {
        <Router<AppRoute, ()>
            render = Router::render(|route: AppRoute| {
                match route {
                    AppRoute::Active => html! { <Index filter=AppFilter::Active /> },
                    AppRoute::Complete => html! { <Index filter=AppFilter::Complete /> },
                    AppRoute::Index => html! { <Index filter=AppFilter::All /> },
                }
            })
        />
    }
}
