#![recursion_limit = "512"]

use wasm_bindgen::prelude::{wasm_bindgen, JsValue};
use yew;

mod app;
mod components;
mod routes;
mod views;

#[wasm_bindgen(start)]
pub fn run() -> Result<(), JsValue> {
    yew::start_app::<app::App>();

    Ok(())
}
